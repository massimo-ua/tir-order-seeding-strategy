const SeedingError = require('./OrderSeedingError');

class TirSendOrderError extends SeedingError {
    constructor(errors) {
        super({
            message: 'SENDING_ORDER_FAILED',
            errors,
        });
    }
}

module.exports = TirSendOrderError;