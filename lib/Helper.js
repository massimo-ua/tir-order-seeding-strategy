const {
    SendOrderError,
} = require('./errors');

const TirOrderHelper = {
    async serializeSendOrderResponse(promise) {
        try {
            const response = await promise;
            const { data = {}, errors } = response;
            if (data.order) {
                return data.order;
            } else {
                throw new SendOrderError(errors);
            }
        } catch(errors) {
            throw new SendOrderError(errors);
        }
    }
}

module.exports = TirOrderHelper;