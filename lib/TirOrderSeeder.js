const {
    serializeSendOrderResponse,
} = require('./Helper');

class TirOrderSeeder {
    constructor(client) {
        this.client = client;
    }

    send(profileId, order) {
        const response = this.client.addOrder(profileId, order);
        return serializeSendOrderResponse(response);
    }
}

module.exports = TirOrderSeeder;