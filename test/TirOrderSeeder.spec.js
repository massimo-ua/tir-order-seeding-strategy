const TirOrderSeeder = require('../lib/TirOrderSeeder');
const { SendOrderError } = require('../lib/errors');

const client = {
    addOrder: async () => {}
};

describe('TirOrderSeeder', () => {
    let seeder;

    beforeEach(() => {
        seeder = new TirOrderSeeder(client);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should create TirOrderSeeder instance', () => {
        expect(seeder).toBeDefined();
    });

    it('should call client.addOrder with profileId and order on send', async () => {
        const spy = spyOn(client, 'addOrder').and.returnValue(Promise.resolve({ data: { order: 1 } }));
        await seeder.send(0, {});
        expect(spy).toHaveBeenCalledWith(0, {});
    });

    it('should return order id on send', async () => {
        spyOn(client, 'addOrder').and.returnValue(Promise.resolve({ data: { order: 1 } }));
        const id = await seeder.send(0, {});
        expect(id).toEqual(1);
    });

    it('should throw error if request failed on send', done => {
        spyOn(client, 'addOrder').and.returnValue(Promise.reject({}));
        seeder.send({}).catch(error => {
            expect(error).toBeInstanceOf(SendOrderError);
            done();
        });
    });

    it('should throw error if request returned response with errors on send', done => {
        spyOn(client, 'addOrder').and.returnValue(Promise.resolve({ errors: {
            name: 'validation failed'
        }}));
        seeder.send({}).catch(error => {
            expect(error).toBeInstanceOf(SendOrderError);
            done();
        });
    });
});